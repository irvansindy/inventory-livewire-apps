<!DOCTYPE html>
<html>
    <head>
        <title>Procurement Detail</title>
    </head>
    <style type="text/css">
        body{
            font-family: 'Roboto Condensed', sans-serif;
        }
        .m-0{
            margin: 0px;
        }
        .p-0{
            padding: 0px;
        }
        .pt-5{
            padding-top:5px;
        }
        .mt-10{
            margin-top:10px;
        }
        .text-center{
            text-align:center !important;
        }
        .w-100{
            width: 100%;
        }
        .w-50{
            width:50%;   
        }
        .w-85{
            width:85%;   
        }
        .w-15{
            width:15%;   
        }
        .w-33{
            width:33%;   
        }
        .logo img{
            width:200px;
            height:100px;
            /* padding-top:30px; */
            padding-bottom: 24px;
        }
        .logo span{
            margin-left:8px;
            top:19px;
            position: absolute;
            font-weight: bold;
            font-size:25px;
        }
        .assign img{
            width:64px;
            height:48px;
            /* padding-top:30px; */
            /* padding-bottom: 24px; */
        }
        .gray-color{
            color:#5D5D5D;
        }
        .text-bold{
            font-weight: bold;
        }
        .border{
            border:1px solid black;
        }
        table tr,th,td{
            border: 1px solid #d2d2d2;
            border-collapse:collapse;
            padding:7px 8px;
        }
        table tr th{
            background: #F4F4F4;
            font-size:15px;
        }
        table tr td{
            font-size:13px;
        }
        table{
            border-collapse:collapse;
        }
        .box-text p{
            line-height:10px;
        }
        .float-left{
            float:left;
        }
        .total-part{
            font-size:16px;
            line-height:12px;
        }
        .total-right p{
            padding-right:20px;
        }
    </style>
    <body>
        <div class="head-title">
            <h1 class="text-center m-0 p-0">PT. Pralon Indonesia</h1>
        </div>
        <div class="add-detail mt-10">
            <div class="w-50 float-left logo mt-10">
                <img src="https://pralon.co.id/wp-content/uploads/2018/06/logo-pralon_white-1024x422.png" alt="Pralon Logo"/>
            </div>
            <div class="w-50 float-left mt-10">
                <p class="m-0 pt-5 text-bold w-100">ICT Item Request Form</p>
                <p class="m-0 pt-5 text-bold w-100">PT. Pralon</p>
                <p class="m-0 pt-5 text-bold w-100">Cimanggis</p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div class="table-section bill-tbl w-100 mt-10">
            <table class="table w-100 mt-10">
                <tr>
                    <td align="center">
                        <div class="box-text">
                            <p>Document Version : <span>04/ICT/08.2018 Rev.00</span></p>
                            <p>Issued Date : <span>{{ date('D, M d Y', strtotime($allDataProcurement[0][0]->procurement->procurementDate)) }}</span></p>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="table-section bill-tbl w-100 mt-10">
            <table class="table w-100 mt-10">
                <tr>
                    <td colspan="3">
                        <div class="box-text">
                            <p>Form Number : <span>{{ $allDataProcurement[0][0]->procurement->procurementCode }}</span></p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="w-50">Number</th>
                    <th class="w-50">Item & Specification</th>
                    <th class="w-50">Quantity</th>
                </tr>
                @forelse ($allDataProcurement[0] as $itemProcurement)
                @php
                    $no = 1;
                @endphp
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>
                            {{ $itemProcurement->inventoryName }} - <span>{{ $itemProcurement->specification }}</span>
                        </td>
                        <td>{{ $itemProcurement->quantity }}</td>
                    </tr>
                @empty
                    <td colspan="3">No Data</td>
                @endforelse
                </tr>
            </table>
        </div>
        <div class="table-section bill-tbl w-100 mt-10">
            <table class="table w-100 mt-10">
                @if ($allDataProcurement[1]->count() > 1)
                <tr>
                    <th class="w-50">
                        Approved by, <span>{{ $allDataProcurement[1][1]->user->name }}</span>
                    </th>
                    <th class="w-50">
                        Confirmed by, <span>{{ $allDataProcurement[1][0]->user->name }}</span>
                    </th>
                    <th class="w-50">
                        Created by, <span>{{ $allDataProcurement[1][0]->procurement->user->name }}</span>
                    </th>
                </tr>
                <tr>
                    <td>
                        Approved at: <span>{{ $allDataProcurement[1][1]->created_at }}</span>
                    </td>
                    <td align="center">
                        Confirmed at: <span>{{ $allDataProcurement[1][0]->created_at }}</span>
                    </td>
                    <td align="center">
                        Created at: <span>{{ $allDataProcurement[1][0]->procurement->created_at }}</span>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <img class="assign" height="80" width="96" alt="signature" src="upload/images/signature/{{$allDataProcurement[1][1]->signature }}">
                    </td>
                    <td align="center">
                        <img class="assign" height="80" width="96" alt="signature" src="upload/images/signature/{{$allDataProcurement[1][0]->signature }}">
                    </td>
                    <td align="center">
                        <img class="assign" height="80" width="96" alt="signature" src="upload/images/signature/{{ $allDataProcurement[1][0]->procurement->procurementSignatureUser }}">
                    </td>
                </tr>
                @elseif($allDataProcurement[1]->count() == 1)
                <tr>
                    <th class="w-50">
                        Confirmed by, <span>{{ $allDataProcurement[1][0]->user->name }}</span>
                    </th>
                    <th class="w-50">
                        Created by, <span>{{ $allDataProcurement[1][0]->procurement->user->name }}</span>
                    </th>
                </tr>
                <tr>
                    <td align="center">
                        Approved at: <span>{{ $allDataProcurement[1][0]->created_at }}</span>
                    </td>
                    <td align="center">
                        Created at: <span>{{ $allDataProcurement[1][0]->procurement->created_at }}</span>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <img class="assign" height="80" width="96" alt="signature" src="upload/images/signature/{{$allDataProcurement[1][0]->signature }}">
                    </td>
                    <td align="center">
                        <img class="assign" height="80" width="96" alt="signature" src="upload/images/signature/{{ $allDataProcurement[1][0]->procurement->procurementSignatureUser }}">
                    </td>
                </tr>
                @endif
            </table>
        </div>
    </body>
</html>