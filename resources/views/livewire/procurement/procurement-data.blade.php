<div>
    <x-slot name="header">
        <h2 class="text-left">List Data Procurement</h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('sweetalert::alert')
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                {{-- list data table --}}
                <div>
                    @if (Auth::user()->roles !== 'SUPERADMIN')
                        <button wire:click="openModal" class="bg-blue-600 hover:bg-blue-800 text-white font-bold py-2 px-4 mb-4 rounded">Make Procurement</button>
                        <input class="form-control mb-3 rounded" type="text" wire:model="search" placeholder="Search" aria-label="search">
                    @else
                        <input class="form-control mb-3 rounded" type="text" wire:model="search" placeholder="Search" aria-label="search">
                    @endif
                    @if($isModalOpen)
                        @include('livewire.procurement.form-procurement-data')
                    @elseif($isDetailProcurement)
                        @include('livewire.procurement.detail-procurement')
                    @elseif($isDetailApproveModalOpen)
                        @include('livewire.procurement.form-approve-procurement')
                    @elseif($isDoneModalOpen)
                        @include('livewire.procurement.done-procurement-data')
                    @elseif($isEditDetailProcurementModalOpen)
                        @include('livewire.procurement.edit-detail-procurement')
                    @elseif($isEditSupplierProcurementModalOpen)
                        @include('livewire.procurement.edit-supplier-procurement')
                    @endif
                    <table class="table-auto w-full">
                        <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2">Procurement Code</th>
                                <th class="px-4 py-2">User</th>
                                <th class="px-4 py-2">Total Price</th>
                                <th class="px-4 py-2">Status</th>
                                <th class="px-4 py-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($procurements as $procurement)
                            <tr>
                                <td class="border px-4 py-2">{{ $procurement->procurementCode }}</td>
                                <td class="border px-4 py-2">{{ $procurement->user->name }}</td>
                                <td class="border px-4 py-2">Rp.{{ number_format($procurement->totalPrice, 2, ',','.') }}</td>
                                <td class="border px-4 py-2">{{ $procurement->status }}</td>
                                <td class="border px-4 py-2">
                                    @if ($procurement->totalPrice > 5000000)
                                        @can('admin')
                                        @if ($procurement->procurementApprovals->where('userId', Auth::user()->id)->where('status', 'WAITING')->first())
                                            <button wire:click="detailApproval({{ $procurement->id }})" class="bg-violet-600 hover:bg-violet-800 text-white font-bold py-2 px-4 rounded">Approve</button>
                                        {{-- @elseif($procurement->status == 'ON PROGRESS')
                                            <button wire:click="doneProcurement({{ $procurement->id }})" class="bg-indigo-600 hover:bg-indigo-800 text-white font-bold py-2 px-4 rounded">Claim</button> --}}
                                        @endif
                                        @endcan
                                    @elseif($procurement->totalPrice < 5000000)
                                        @can('onlyAdmin')
                                        @if ($procurement->procurementApprovals->where('userId', Auth::user()->id)->where('status', 'WAITING')->first())
                                            <button wire:click="detailApproval({{ $procurement->id }})" class="bg-violet-600 hover:bg-violet-800 text-white font-bold py-2 px-4 rounded">Approve</button>
                                        {{-- @elseif($procurement->status == 'ON PROGRESS')
                                            <button wire:click="doneProcurement({{ $procurement->id }})" class="bg-indigo-600 hover:bg-indigo-800 text-white font-bold py-2 px-4 rounded">Claim</button> --}}
                                        @endif
                                        @endcan
                                    @endif
                                    @can('userProcurement')
                                        <button wire:click="doneProcurement({{ $procurement->id }})" class="bg-indigo-600 hover:bg-indigo-800 text-white font-bold py-2 px-4 rounded">Claim</button>
                                    @endcan
                                    <button wire:click="detailProcurement({{ $procurement->id }})" class="bg-sky-600 hover:bg-sky-800 text-white font-bold py-2 px-4 rounded">Detail</button>
                                    @if ($procurement->status == 'ON PROGRESS')
                                        <button wire:click="printProcurement({{ $procurement->id }})" class="bg-emerald-600 hover:bg-emerald-800 text-white font-bold py-2 px-4 rounded">Print</button>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="7" class="border px-4 py-2"><p class="text-justify-center hover:font-bold">No Data</p></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="px-4 mt-4">
                        {{$procurements->links()}}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
