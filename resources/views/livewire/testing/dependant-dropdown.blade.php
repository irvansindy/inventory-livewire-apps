<div>
    <x-slot name="header">
        <h2 class="text-left hover:font-bold">Testing Dependant Dropdown</h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                @include('sweetalert::alert')
                <div class="mb-4 px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="mb-4">
                        <label for="selectedOffice"
                            class="block text-gray-700 text-sm font-bold mb-2">Location</label>
                        <select class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="selectedOffice" wire:model="selectedOffice">
                            <option value="hidden">Select Location</option>
                            @forelse ($allDataLocation as $location)
                            <option value="{{ $location->id }}">{{ $location->id }} - {{ $location->officeName }}</option>
                            @empty
                            <option value="hidden">Data doesn't exists</option>
                            @endforelse
                        </select>
                        @error('selectedOffice') <span class="text-red-500">{{ $message }}</span>@enderror
                        
                    </div>
                    @if (!is_null($selectedOffice))
                        <div>
                            <label for="userId"
                            class="block text-gray-700 text-sm font-bold mb-2">User</label>
                            <select class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="userId" wire:model="userId">
                                {{-- <option value="hidden">Select User</option> --}}
                                @forelse ( $userId as $user )
                                    <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->name }}</option>
                                @empty
                                <option value="hidden">Data doesn't exists</option>
                                @endforelse
                            </select>
                            @error('userId') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
