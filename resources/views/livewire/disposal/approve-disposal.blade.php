<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-screen-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                @csrf
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <h5 class="text-xl font-semibold leading-normal text-gray-800 mb-2">Disposal Detail</h5>
                    <div class="border-t border-gray-300"></div>
                    <div class="grid grid-cols-2 gap-4 mt-4">
                        <div class="col-span-1">
                            <div class="flex items-center mb-2">
                                <div class="flex-shrink-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                    </svg>
                                </div>
                                <div class="ml-3">
                                    <p class="text-md leading-5 font-medium text-gray-900">
                                        Disposal Number
                                    </p>
                                    <p class="text-sm leading-5 text-gray-500">
                                        {{ $this->disposalNumber }}
                                    </p>
                                </div>
                            </div>
                            <div class="flex items-center mb-2">
                                <div class="flex-shrink-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                    </svg>
                                </div>
                                <div class="ml-3">
                                    <p class="text-md leading-5 font-medium text-gray-900">
                                        Disposal Date
                                    </p>
                                    <p class="text-sm leading-5 text-gray-500">
                                        {{ $this->disposalDate }}
                                    </p>
                                </div>
                            </div>
                            <div class="flex items-center mb-2">
                                <div class="flex-shrink-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                    </svg>
                                </div>
                                <div class="ml-3">
                                    <p class="text-md leading-5 font-medium text-gray-900">
                                        Total Inventory
                                    </p>
                                    <p class="text-sm leading-5 text-gray-500">
                                        {{ $this->inventory }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-span-1">
                            <div class="flex items-center mb-2">
                                <div class="flex-shrink-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                    </svg>
                                </div>
                                <div class="ml-3">
                                    <p class="text-md leading-5 font-medium text-gray-900">
                                        User
                                    </p>
                                    <p class="text-sm leading-5 text-gray-500">
                                        {{ $this->userId }}
                                    </p>
                                </div>
                            </div>
                            <div class="flex items-center mb-2">
                                <div class="flex-shrink-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                    </svg>
                                </div>
                                <div class="ml-3">
                                    <p class="text-md leading-5 font-medium text-gray-900">
                                        Status
                                    </p>
                                    <p class="text-sm leading-5 text-gray-500">
                                        {{ $this->status }}
                                    </p>
                                </div>
                            </div>
                            <div class="flex items-center mb-2">
                                <div class="flex-shrink-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                    </svg>
                                </div>
                                <div class="ml-3">
                                    <p class="text-md leading-5 font-medium text-gray-900">
                                        Description
                                    </p>
                                    <p class="text-sm leading-5 text-gray-500">
                                        {{ $this->description }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2">
                        <table class="table-auto w-full font-medium">
                            <thead>
                                <tr class="bg-gray-100">
                                    <th class="px-4 py-2">Inventory Name</th>
                                    <th class="px-4 py-2">Specification</th>
                                    <th class="px-4 py-2">Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($this->dataDisposalDetail as $item)
                                    <tr>
                                        <td class="px-4 py-2">{{ $item->inventory->inventoryName }}</td>
                                        <td class="px-4 py-2">{{ $item->inventory->specification }}</td>
                                        <td class="px-4 py-2">Rp. {{ number_format($item->inventory->productPrice, 2, ',','.') }}</td>
                                    </tr>
                                @empty
                                <tr>
                                    <td colspan="2" class="text-center mt-4">No Data</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="border-t border-gray-300 my-2"></div>
                    {{-- form approval --}}
                    <div class="mt-4">
                        <h5 class="text-xl font-semibold leading-normal text-gray-800 mb-2">Disposal Stagging Approval</h5>
                        <div class="grid grid-cols-2 my-4">
                            <div>
                                <div class="mr-4">
                                    <label for="approvalComment"
                                        class="block text-gray-700 text-sm font-bold mb-2">Note or Comment</label>
                                    <textarea
                                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="approvalComment" wire:model="approvalComment" placeholder="Note or Comment">
                                    </textarea>
                                    @error('approvalComment') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div>
                                <div class="ml-4">
                                    <x-signature-pad wire:model.defer="approvalSignature"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end form approval --}}
                </div>
                <div class="border-t border-gray-300"></div>
                <div class="px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="storeAndUpdateApproveDisposal" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-sky-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-sky-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            <svg wire:loading.delay wire:target="storeAndUpdateApproveDisposal" class="w-5 h-5 mr-3 -ml-1 text-white animate-spin" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                                <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                            </svg>
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="cancelApproveDisposal" type="button" class="inline-flex justify-center w-full rounded-md border border-red-300 px-4 py-2 bg-white text-base leading-6 font-bold text-red-500 shadow-sm hover:text-red-800 focus:outline-none focus:border-red-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>