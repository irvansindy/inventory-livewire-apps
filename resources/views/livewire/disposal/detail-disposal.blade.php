<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-screen-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <h5 class="text-xl font-semibold leading-normal text-gray-800 mb-2">Disposal Detail</h5>
                <div class="border-t border-gray-300"></div>
                <div class="grid grid-cols-2 gap-4 mt-4">
                    <div class="col-span-1">
                        <div class="flex items-center mb-2">
                            <div class="flex-shrink-0">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                            <div class="ml-3">
                                <p class="text-md leading-5 font-medium text-gray-900">
                                    Disposal Number
                                </p>
                                <p class="text-sm leading-5 text-gray-500">
                                    {{ $this->disposalNumber }}
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center mb-2">
                            <div class="flex-shrink-0">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                            <div class="ml-3">
                                <p class="text-md leading-5 font-medium text-gray-900">
                                    Disposal Date
                                </p>
                                <p class="text-sm leading-5 text-gray-500">
                                    {{ $this->disposalDate }}
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center mb-2">
                            <div class="flex-shrink-0">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                            <div class="ml-3">
                                <p class="text-md leading-5 font-medium text-gray-900">
                                    Total Inventory
                                </p>
                                <p class="text-sm leading-5 text-gray-500">
                                    {{ $this->inventory }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-1">
                        <div class="flex items-center mb-2">
                            <div class="flex-shrink-0">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                            <div class="ml-3">
                                <p class="text-md leading-5 font-medium text-gray-900">
                                    User
                                </p>
                                <p class="text-sm leading-5 text-gray-500">
                                    {{ $this->userId }}
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center mb-2">
                            <div class="flex-shrink-0">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                            <div class="ml-3">
                                <p class="text-md leading-5 font-medium text-gray-900">
                                    Status
                                </p>
                                <p class="text-sm leading-5 text-gray-500">
                                    {{ $this->status }}
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center mb-2">
                            <div class="flex-shrink-0">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                            <div class="ml-3">
                                <p class="text-md leading-5 font-medium text-gray-900">
                                    Description
                                </p>
                                <p class="text-sm leading-5 text-gray-500">
                                    {{ $this->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2">
                    <table class="table-auto w-full font-medium">
                        <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2">Inventory Name</th>
                                <th class="px-4 py-2">Specification</th>
                                <th class="px-4 py-2">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($this->dataDisposalDetail as $item)
                                <tr>
                                    <td class="px-4 py-2">{{ $item->inventory->inventoryName }}</td>
                                    <td class="px-4 py-2">{{ $item->inventory->specification }}</td>
                                    <td class="px-4 py-2">Rp. {{ number_format($item->inventory->productPrice, 2, ',','.') }}</td>
                                </tr>
                            @empty
                            <tr>
                                <td colspan="2" class="text-center mt-4">No Data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="border-t border-gray-300"></div>
            <div class="px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                    <button wire:click="closeDetailDisposal" type="button" class="inline-flex justify-center w-full rounded-md border border-red-300 px-4 py-2 bg-white text-base leading-6 font-bold text-red-500 shadow-sm hover:text-red-800 focus:outline-none focus:border-red-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                        Close
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>