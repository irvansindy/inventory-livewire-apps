<div>
    <x-slot name="header">
        <h2 class="text-left">List Data Disposal</h2>
    </x-slot>
    <div class="py-12" x-data="{ open: false }">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                @include('sweetalert::alert')
                <div class="grid grid-cols-2">
                    <div>
                        @can('allUser')
                            <button wire:click="createDisposal()" class="bg-blue-600 hover:bg-blue-800 text-white font-bold py-2 px-4 mb-4 rounded">Make Disposal</button>
                        @endcan
                        <input class="form-control mb-3 rounded" type="text" wire:model="search" placeholder="Search" aria-label="search">
                    </div>
                    <div></div>
                    {{-- <div class="flex flex-row-reverse">
                        <button wire:click="exportPDF" class="bg-cyan-600 hover:bg-cyan-800 text-white font-bold py-2 px-4 mb-4 rounded place-items-end">Export PDF</button>
                        <button wire:click="exportCSV" class="bg-cyan-600 hover:bg-cyan-800 text-white font-bold py-2 px-4 mb-4 rounded place-items-end mr-2">Export CSV</button>
                    </div> --}}
                </div>
                @if($isModalCreateOpen)
                    @include('livewire.disposal.form-create-disposal')
                @elseif ($isModalDetailOpen)
                    @include('livewire.disposal.detail-disposal')
                @elseif ($isModalApproveOpen)
                    @include('livewire.disposal.approve-disposal')
                @endif
                <table class="table-auto w-full">
                    <thead>
                        <tr class="bg-gray-100">
                            <th class="px-4 py-2">Disposal Number</th>
                            <th class="px-4 py-2">User Request</th>
                            <th class="px-4 py-2">Date</th>
                            <th class="px-4 py-2">Total Inventory</th>
                            <th class="px-4 py-2">Status</th>
                            <th class="px-4 py-2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($disposals as $item)
                            <tr>
                                <td class="border px-4 py-2">{{ $item->disposalNumber }}</td>
                                <td class="border px-4 py-2">{{ $item->user->name }}</td>
                                <td class="border px-4 py-2">{{ $item->disposalDate }}</td>
                                <td class="border px-4 py-2">{{ $item->totalInventory }}</td>
                                <td class="border px-4 py-2">{{ $item->status }}</td>
                                <td class="border px-4 py-2">
                                    <button wire:click="detailDisposal({{ $item->id }})" class="bg-sky-600 hover:bg-sky-800 text-white font-bold py-2 px-4 mb-4 rounded">Detail</button>
                                    @can('admin')
                                        <button wire:click="approveDisposal({{ $item->id }})" class="bg-emerald-600 hover:bg-emerald-800 text-white font-bold py-2 px-4 mb-4 rounded">Approve</button>
                                    @endcan
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center mt-4">No Data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="px-4 mt-4">
                    {{$disposals->links()}}
                </div>
            </div>
        </div>
        <div x-show="open" x-transition>
            <x-signature-pad wire:model.defer="approvalSignature"/>
        </div>
    </div>
</div>
