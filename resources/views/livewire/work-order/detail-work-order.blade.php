<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-2xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <div class="px-4 pt-5 pb-4 sm:p-6 sm:pb-4 w-full">
                <h5 class="text-2xl font-bold leading-tight text-gray-900">
                Work Order Detail
                </h5>
            </div>
            <div class="border-t border-gray-300"></div>
            <div class="mb-4 px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div class="grid grid-cols-2">
                    <div>
                        <table class="table-auto w-full">
                            <tbody>
                                <tr>
                                    <td class="px-4 py-2">Number</td>
                                    <td class="px-4 py-2">:</td>
                                    <td class="px-4 py-2">{{ $this->dataWODetail->workOrderNumber }}</td>
                                </tr>
                                <tr>
                                    <td class="px-4 py-2">Type</td>
                                    <td class="px-4 py-2">:</td>
                                    <td class="px-4 py-2">{{ $this->dataWODetail->workOrderTypes->workOrderType }}</td>
                                </tr>
                                <tr>
                                    <td class="px-4 py-2">Status</td>
                                    <td class="px-4 py-2">:</td>
                                    <td class="px-4 py-2">{{ $this->dataWODetail->workOrderStatus }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <table class="table-auto w-full">
                            <tbody>
                                <tr>
                                    <td class="px-4 py-2">Request by</td>
                                    <td class="px-4 py-2">:</td>
                                    <td class="px-4 py-2">{{ $this->dataWODetail->userRequestedBy->name }}</td>
                                </tr>
                                <tr>
                                    <td class="px-4 py-2">Approved by</td>
                                    <td class="px-4 py-2">:</td>
                                    <td class="px-4 py-2">{{ $this->nameApproval->count() > 0 ? $this->dataWODetail->workOrderApprovals[0]->user->name : 'Waiting For Approve' }}</td>
                                </tr>
                                <tr>
                                    <td class="px-4 py-2">Handled by</td>
                                    <td class="px-4 py-2">:</td>
                                    <td class="px-4 py-2">{{ $this->nameHandler->count() > 0 ? $this->dataWODetail->workOrderHandlers[0]->user->name: 'Waiting For Handling' }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mt-4">
                    <label for="workOrderDescription"
                        class="block text-gray-700 text-sm font-bold mb-2">Detail Description</label>
                    <textarea
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="workOrderDescription" wire:model="workOrderDescription" readonly></textarea>
                    @error('workOrderDescription') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                    <button wire:click="closeDetailWorkOrder()" type="button" class="inline-flex justify-center w-full rounded-md border border-red-300 px-4 py-2 bg-white text-base leading-6 font-bold text-red-500 shadow-sm hover:text-red-800 focus:outline-none focus:border-red-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                        Close
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>