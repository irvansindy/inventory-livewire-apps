<div>
    <x-slot name="header">
        <h2 class="text-left">List Data Work Order</h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                @include('sweetalert::alert')
                <div class="grid grid-cols-2">
                    <div>
                        @can('onlyUser')
                            <button wire:click="createWorkOrder()" class="bg-blue-600 hover:bg-blue-800 text-white font-bold py-2 px-4 mb-4 rounded">Make Work Order</button>
                        @endcan
                        <input class="form-control mb-3 rounded" type="text" wire:model="search" placeholder="Search" aria-label="search">
                    </div>
                    <div></div>
                    {{-- <div class="flex flex-row-reverse">
                        <button wire:click="exportPDF" class="bg-cyan-600 hover:bg-cyan-800 text-white font-bold py-2 px-4 mb-4 rounded place-items-end">Export PDF</button>
                        <button wire:click="exportCSV" class="bg-cyan-600 hover:bg-cyan-800 text-white font-bold py-2 px-4 mb-4 rounded place-items-end mr-2">Export CSV</button>
                    </div> --}}
                </div>
                @if($isModalCreateOpen)
                    @include('livewire.work-order.form-add-work-order')
                @elseif($isModalDetailOpen)
                    @include('livewire.work-order.detail-work-order')
                @elseif($isModalApprovalOpen)
                    @include('livewire.work-order.approve-work-order')
                @elseif($isModalHandlingOpen)
                    @include('livewire.work-order.handle-work-order')
                @endif
                <table class="table-auto w-full">
                    <thead>
                        <tr class="bg-gray-100">
                            <th class="px-4 py-2">WO Number</th>
                            <th class="px-4 py-2">WO Type</th>
                            <th class="px-4 py-2">User Request</th>
                            <th class="px-4 py-2">Date</th>
                            <th class="px-4 py-2">WO Status</th>
                            <th class="px-4 py-2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($dataWO as $item)
                            <tr>
                                <td class="border px-4 py-2">{{ $item->workOrderNumber }}</td>
                                <td class="border px-4 py-2">{{ $item->workOrderTypes->workOrderType }}</td>
                                <td class="border px-4 py-2">{{ $item->userRequestedBy->name }}</td>
                                <td class="border px-4 py-2">{{ $item->workOrderDateRequest }}</td>
                                <td class="border px-4 py-2">{{ $item->workOrderStatus }}</td>
                                <td class="border px-4 py-2">
                                    @can('onlyAdmin')
                                        @if ($item->workOrderStatus == 'PENDING')
                                            <button wire:click="approveWorkOrder({{ $item->id }})" class="bg-emerald-600 hover:bg-emerald-800 text-white font-bold py-2 px-4 mb-4 rounded">Assign To</button>
                                        @endif
                                    @endcan
                                    @can('userHandler')
                                        @if ($item->workOrderStatus == 'ON PROGRESS')
                                            <button wire:click="handlingWorkOrder({{ $item->id }})" class="bg-indigo-600 hover:bg-indigo-800 text-white font-bold py-2 px-4 mb-4 rounded">Done</button>
                                        @endif
                                    @endcan
                                    <button wire:click="detailWorkOrder({{ $item->id }})" class="bg-sky-600 hover:bg-sky-800 text-white font-bold py-2 px-4 mb-4 rounded">Detail</button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center mt-4">No Data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="px-4 mt-4">
                    {{$dataWO->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
