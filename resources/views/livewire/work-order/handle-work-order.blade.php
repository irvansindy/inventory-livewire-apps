<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-2xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <div class="px-4 pt-5 pb-4 sm:p-6 sm:pb-4 w-full">
                <h5 class="text-2xl font-bold leading-tight text-gray-900">
                Work Order Detail
                </h5>
            </div>
            <div class="border-t border-gray-300"></div>
            <form>
                @csrf
                <div class="mb-4 px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="grid grid-cols-2">
                        <div>
                            <table class="table-auto w-full">
                                <tbody>
                                    <tr>
                                        <td class="px-4 py-2">Number</td>
                                        <td class="px-4 py-2">:</td>
                                        <td class="px-4 py-2">{{ $this->dataWODetail->workOrderNumber }}</td>
                                    </tr>
                                    <tr>
                                        <td class="px-4 py-2">Type</td>
                                        <td class="px-4 py-2">:</td>
                                        <td class="px-4 py-2">{{ $this->dataWODetail->workOrderTypes->workOrderType }}</td>
                                    </tr>
                                    <tr>
                                        <td class="px-4 py-2">Status</td>
                                        <td class="px-4 py-2">:</td>
                                        <td class="px-4 py-2">{{ $this->dataWODetail->workOrderStatus }}</td>
                                    </tr>
                                    <tr>
                                        <td class="px-4 py-2">Detail Description</td>
                                        <td class="px-4 py-2">:</td>
                                        <td class="px-4 py-2">{{ $this->dataWODetail->workOrderDescription }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <table class="table-auto w-full">
                                <tbody>
                                    <tr>
                                        <td class="px-4 py-2">Request by</td>
                                        <td class="px-4 py-2">:</td>
                                        <td class="px-4 py-2">{{ $this->dataWODetail->userRequestedBy->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="px-4 py-2">Approved by</td>
                                        <td class="px-4 py-2">:</td>
                                        <td class="px-4 py-2">{{ $this->nameApproval->count() > 0 ? $this->dataWODetail->workOrderApprovals[0]->user->name : 'Waiting For Approve' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="px-4 py-2">Handled by</td>
                                        <td class="px-4 py-2">:</td>
                                        <td class="px-4 py-2">{{ $this->nameHandler->count() > 0 ? $this->dataWODetail->workOrderHandlers[0]->user->name: 'Waiting For Handling' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="mt-4">
                            <label for="workOrderDescription"
                                class="block text-gray-700 text-sm font-bold mb-2">Detail Description</label>
                            <textarea
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="workOrderDescription" wire:model="workOrderDescription" readonly></textarea>
                            @error('workOrderDescription') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="my-4">
                            <label for="workOrderTroubleShootingNote"
                                class="block text-gray-700 text-sm font-bold mb-2">Note Trouble Shooting</label>
                            <textarea
                                class="shadow appearance-none border rounded w-full h-auto py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="workOrderTroubleShootingNote" wire:model="workOrderTroubleShootingNote"
                                placeholder="Enter Note Trouble Shooting" style="height: 170px"></textarea>
                            @error('workOrderTroubleShootingNote') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="storeAndUpdateHandlingWorkOrder()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-sky-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-sky-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            <svg wire:loading.delay wire:target="storeAndUpdateHandlingWorkOrder()" class="w-5 h-5 mr-3 -ml-1 text-white animate-spin" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                                <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                            </svg>
                            Approve
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeHandlingWorkOrder()" type="button" class="inline-flex justify-center w-full rounded-md border border-red-300 px-4 py-2 bg-white text-base leading-6 font-bold text-red-500 shadow-sm hover:text-red-800 focus:outline-none focus:border-red-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>