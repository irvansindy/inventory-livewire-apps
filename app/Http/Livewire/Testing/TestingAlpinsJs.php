<?php

namespace App\Http\Livewire\Testing;

use Illuminate\Support\Str;
use Livewire\Component;
use App\Models\Office;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class TestingAlpinsJs extends Component
{
    public $officeId;
    public $userId;
    public $allDataLocation;

    public $selectedOffice = null;
    
    public function mount()
    {
        $this->allDataLocation = Office::all();
        $this->userId = collect();
        // $this->userId = [];
        // dd($this->userId);
    }
    
    public function render()
    {
        // return view('livewire.testing.signature-pad-base64');
        return view('livewire.testing.dependant-dropdown');
    }

    public function updatedSelectedOffice($location)
    {
        if(!is_null($location)) {
            $this->userId = User::where('officeId', $location)->get();
        }
    }
}
