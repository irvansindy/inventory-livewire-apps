<?php

namespace App\Http\Livewire\WorkOrder;

use Livewire\Component;
use App\Models\User;
use App\Models\WorkOrder;
use App\Models\WorkOrderTypes;
use App\Models\WorkOrderApproval;
use App\Models\WorkOrderHandler;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;

class WorkOrderData extends Component
{
    // data binding WO Master
    public $workOrderId, $workOrderNumber, $workOrderType, $workOrderStatus, $workOrderDescription, $userIdRequestedBy, $workOrderNumberRequestToServiceCenter, $workOrderDateRequest, $workOrderTroubleShootingNote, $workOrderDateUpdate;

    // data binding WO Approval
    public $userIdApprovedBy, $dateApproved, $nameApproval;

    // data binding WO Handler
    public $userIdAssignedTo, $dateHandled, $nameHandler;

    // variable for auto number
    public $initialNumber, $firstNumber, $dateNumber, $currentTime;

    public $isModalOpen = 0;
    public $isModalCreateOpen = 0;
    public $isModalDetailOpen = 0;
    public $isModalApprovalOpen = 0;
    public $isModalHandlingOpen = 0;
    public $limitPerPage = 10;
    public $search;

    protected $queryString = ['search'=> ['except' => '']];
    protected $listeners = [
        'workOrders' => 'workOrderPostData'
    ];

    public $allWOType = [];
    public $allUserHandler = [];
    public $dataWODetail = [];

    public function workOrderPostData()
    {
        $this->limitPerPage = $this->limitPerPage+6;
    }

    public function mount()
    {
        $this->allWOType = WorkOrderTypes::all();
        $this->allUserHandler = User::where('roles', '=', 'USER')->where('positionId', '=', 1)->get();
    }

    public function render()
    {
        // $dataWO = WorkOrder::all();
        $dataWO = WorkOrder::with([
            'workOrderTypes',
            'userRequestedBy',
        ])->latest()->paginate($this->limitPerPage);

        if($this->search !== NULL)
        {
            $dataWO = WorkOrder::with([
                'workOrderTypes',
                'userRequestedBy',
            ])->whereHas('workOrderTypes', function($query) {
                $query->where('workOrderType', 'like', '%' . $this->search . '%');
            })->orWhereHas('userRequestedBy', function($query) {
                $query->where('name', 'like', '%' . $this->search . '%');
            })
            ->orWhere('workOrderStatus', 'like', '%' . $this->search . '%')
            ->orWhere('workOrderDateRequest', 'like', '%' . $this->search . '%')
            ->orWhere('workOrderNumber', 'like', '%' . $this->search . '%')
            ->latest()->paginate($this->limitPerPage);
        }
        $this->emit('workOrderPostData');
        return view('livewire.work-order.work-order-data', [
            'dataWO' => $dataWO,
        ]);
    }

    public function createWorkOrder()
    {
        $this->isModalCreateOpen = true;
    }

    public function resetField()
    {
        $this->workOrderType = '';
        $this->workOrderDescription = '';
        $this->workOrderTroubleShootingNote = '';
    }

    public function closeCreteWorkOrder()
    {
        $this->isModalCreateOpen = false;
    }

    public function storeWorkOrder()
    {
        // validation
        $this->validate([
            'workOrderType' => 'required',
            'workOrderDescription' => 'required',
        ], [
            'workOrderType.required' => 'Please select Work Order Type',
            'workOrderDescription.required' => 'Please fill Work Order Description',
        ]);

        // method auto number
        $checkDataWO = WorkOrder::latest()->first();
        
        $this->initialNumber = 'WO';
        $this->dateNumber = Carbon::now()->format('Y-m');
        $this->firstNumber = 1;
        
        // dd($this->currentTime);
        if($checkDataWO == null) {
            $this->workOrderNumber = $this->initialNumber . '/' . $this->dateNumber . '/' . $this->firstNumber;
            // store data
            $workOrderMaster = WorkOrder::create([
                'workOrderNumber' => $this->workOrderNumber,
                'workOrderType' => $this->workOrderType,
                'workOrderStatus' => 'PENDING',
                'workOrderDescription' => $this->workOrderDescription,
                'userIdRequestedBy' => auth()->user()->id,
                'workOrderDateRequest' => Carbon::now()->format('Y-m-d'),
            ]);

            WorkOrderApproval::create([
                'workOrderId' => $workOrderMaster->id,
                'userId' => 2,
                'dateApproved' => Carbon::now(),
            ]);
        } else {
            $checkCurrentWO = substr($checkDataWO->workOrderNumber, 11);
            $convertInt =  (int)$checkCurrentWO + 1;
            
            $checkCurrentWOData = Carbon::parse($checkDataWO->workOrderDateRequest)->format('Y-m');

            if ($this->dateNumber !== $checkCurrentWOData) {
                $this->workOrderNumber = $this->initialNumber . '/' . $this->dateNumber . '/' . $this->firstNumber;
            } else {
                $this->workOrderNumber = $this->initialNumber . '/' . $this->dateNumber . '/' . $convertInt;
            }
            // store data
            $workOrderMaster = WorkOrder::create([
                'workOrderNumber' => $this->workOrderNumber,
                'workOrderType' => $this->workOrderType,
                'workOrderStatus' => 'PENDING',
                'workOrderDescription' => $this->workOrderDescription,
                'userIdRequestedBy' => auth()->user()->id,
                'workOrderDateRequest' => Carbon::now()->format('Y-m-d'),
            ]);

            WorkOrderApproval::create([
                'workOrderId' => $workOrderMaster->id,
                'userId' => 2,
                'dateApproved' => Carbon::now(),
            ]);
        }
        $this->resetField();
        alert()->success('SuccessAlert','Work order has been created successfully.');
        $this->isModalCreateOpen = false;
    }

    public function detailWorkOrder($id)
    {
        $this->dataWODetail = WorkOrder::with([
            'workOrderTypes',
            'userRequestedBy',
            'workOrderApprovals',
            'workOrderHandlers',
        ])->findOrFail($id);
        
        $this->workOrderDescription = $this->dataWODetail->workOrderDescription;
        $this->nameApproval = $this->dataWODetail->workOrderApprovals;
        $this->nameHandler = $this->dataWODetail->workOrderHandlers;
        
        $this->isModalDetailOpen = true;
    }

    public function closeDetailWorkOrder()
    {
        $this->isModalDetailOpen = false;
    }

    public function approveWorkOrder($id)
    {
        $this->dataWODetail = WorkOrder::with([
            'workOrderTypes',
            'userRequestedBy',
            'workOrderApprovals',
            'workOrderHandlers',
        ])->findOrFail($id);
        
        $this->workOrderDescription = $this->dataWODetail->workOrderDescription;
        $this->nameApproval = $this->dataWODetail->workOrderApprovals;
        $this->nameHandler = $this->dataWODetail->workOrderHandlers;
        
        $this->isModalApprovalOpen = true;
    }

    public function closeApproveWorkOrder()
    {
        $this->isModalApprovalOpen = false;
    }

    public function storeApproveWorkOrder()
    {
        Gate::authorize('onlyAdmin');
        // validation
        $this->validate([
            'userIdAssignedTo' => 'required',
        ], [
            'userIdAssignedTo.required' => 'Please select Work Order Handler',
        ]);

        // store data
        WorkOrderHandler::create([
            'workOrderId' => $this->dataWODetail->id,
            'userId' => $this->userIdAssignedTo,
            'dateApproved' => Carbon::now(),
        ]);

        WorkOrder::findOrFail($this->dataWODetail->id)->update([
            'workOrderStatus' => 'ON PROGRESS',
        ]);
        
        $this->isModalApprovalOpen = false;
        alert()->success('SuccessAlert','Work order has been approved successfully.');
    }

    public function handlingWorkOrder($id)
    {
        $this->dataWODetail = WorkOrder::with([
            'workOrderTypes',
            'userRequestedBy',
            'workOrderApprovals',
            'workOrderHandlers',
        ])->findOrFail($id);
        
        $this->workOrderDescription = $this->dataWODetail->workOrderDescription;
        $this->nameApproval = $this->dataWODetail->workOrderApprovals;
        $this->nameHandler = $this->dataWODetail->workOrderHandlers;

        $this->isModalHandlingOpen = true;
    }

    public function closeHandlingWorkOrder()
    {
        $this->isModalHandlingOpen = false;
    }

    public function storeAndUpdateHandlingWorkOrder()
    {
        // validation
        $this->validate([
            'workOrderTroubleShootingNote' => 'required',
        ], [
            'workOrderTroubleShootingNote.required' => 'Please fill the Note',
        ]);

        WorkOrder::where('id', $this->dataWODetail->id)->update([
            'workOrderTroubleShootingNote' => $this->workOrderTroubleShootingNote,
            'workOrderStatus' => 'Done'
        ]);

        WorkOrderHandler::where('workOrderId', $this->dataWODetail->id)->update([
            'dateHandled' => Carbon::now(),
        ]);

        $this->resetField();
        $this->isModalHandlingOpen = false;
        alert()->success('SuccessAlert','Work order has been approved successfully.');
    }
}
