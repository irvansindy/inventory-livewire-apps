<?php

namespace App\Http\Livewire\Disposal;

use Livewire\Component;
use App\Models\Disposals;
use App\Models\DisposalDetails;
use App\Models\ProductInventory;
use App\Models\User;
use App\Models\DisposalApprovals;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class DisposalData extends Component
{
    // data disposal master
    public $disposalNumber, $userId, $disposalDate, $description, $status, $inventory;
    public $totalInventory = [];
    public $allInventoryActive = [];
    // data disposal detail
    public $disposalId, $inventoryId;

    // data disposal approval
    public $disposalApprovalId, $approvalStatus, $approvalComment, $approvalSignature;

    public $isModalCreateOpen = 0;
    public $isModalDetailOpen = 0;
    public $isModalApproveOpen = 0;

    public $limitPerPage = 10;
    public $search;

    public $initialDisposal;
    public $dateNumber;
    public $firstNumber;

    public $dataDisposalDetail = [];
    public $dataDisposalApproval = [];

    protected $queryString = ['search'=> ['except' => '']];
    protected $listeners = [
        'disposals' => 'disposalPostData'
    ];

    public function disposalPostData()
    {
        $this->limitPerPage = $this->limitPerPage+6;
    }

    public function mount()
    {
        $this->allInventoryActive = ProductInventory::all();
    }

    public function resetField()
    {
        $this->description = '';
        $this->totalInventory = [];
    }

    public function render()
    {
        $disposals = Disposals::with([
            'disposalDetails',
            'user',
        ])->latest()->paginate($this->limitPerPage);
        
        // dd($disposals);

        if($this->search !== NULL) {
            $disposals = Disposals::with([
                'disposalDetails',
                'user',
            ])->where('disposalNumber', 'like', '%'.$this->search.'%')
            ->orWhereHas('user', function($query) {
                $query->where('name', 'like', '%' . $this->search . '%');
            })
            ->paginate($this->limitPerPage);
        }
        return view('livewire.disposal.disposal-data', [
            'disposals' => $disposals,
        ]);
    }

    public function createDisposal()
    {
        // $this->allInventoryActive = ProductInventory::onlyTrashed()->get();

        // dd($this->allInventoryActive);
        $this->isModalCreateOpen = true;
    }

    public function cancelCreateDisposal()
    {
        $this->isModalCreateOpen = false;
    }

    public function storeDisposal()
    {
        $this->validate([
            'totalInventory.0' => 'required',
            'totalInventory.*' => 'required',
            'description' => 'required',
        ], [
            'totalInventory.0.required' => 'Total Inventory is required',
            'totalInventory.*.required' => 'Total Inventory is required',
            'description.required' => 'Description is required',
        ]);
        // dd(count($this->totalInventory));
        
        $checkDisposal = Disposals::latest()->first();

        $this->initialDisposal = 'DISP';
        $this->dateNumber = Carbon::now()->format('Y-m');
        $this->firstNumber = 1;

        if($checkDisposal == NULL) {
            $this->disposalNumber = $this->initialDisposal . '/' . $this->dateNumber . '/' . $this->firstNumber;
        } else {
            $checkCurrentDisposal = substr($checkDisposal->disposalNumber, 13);
            $convertInt = (int)$checkCurrentDisposal + 1;

            $checkCurrentDisposalData = Carbon::parse($checkDisposal->disposalDate)->format('Y-m');

            if($this->dateNumber !== $checkCurrentDisposalData) {
                $this->disposalNumber = $this->initialDisposal . '/' . $this->dateNumber . '/' . $this->firstNumber;
            } else {
                $this->disposalNumber = $this->initialDisposal . '/' . $this->dateNumber . '/' . $convertInt;
            }
        }
        // store data
        $disposalMaster = Disposals::create([
            'disposalNumber' => $this->disposalNumber,
            'userId' => Auth()->user()->id,
            'disposalDate' => Carbon::now()->format('Y-m-d'),
            'description' => $this->description,
            'totalInventory' => count($this->totalInventory),
            'status' => 'PENDING',
        ]);

        foreach ($this->totalInventory as $value) {
            DisposalDetails::create([
                'disposalId' => $disposalMaster->id,
                'inventoryId' => $value,
            ]);
        }

        DisposalApprovals::create([
            'disposalId' => $disposalMaster->id,
            'userId' => Auth()->user()->parentUserId,
            'status' => 'PENDING',
            'comment' => '',
            'signature' => '',
        ]);

        $this->resetField();
        alert()->success('SuccessAlert','Disposal has been created successfully.');
        $this->isModalCreateOpen = false;

    }

    public function detailDisposal($id)
    {
        // data disposal master
        $dataDisposal = Disposals::with([
            'user',
        ])->find($id);

        // dd($dataDisposal);
        // $this->disposalId = $id;
        $this->disposalNumber = $dataDisposal->disposalNumber;
        $this->disposalDate = $dataDisposal->disposalDate;
        $this->description = $dataDisposal->description;
        $this->status = $dataDisposal->status;
        $this->userId = $dataDisposal->user->name;
        $this->inventory = $dataDisposal->totalInventory;

        // data disposal detail
        $dataDisposalDetail = DisposalDetails::with([
            'inventory',
            'disposal'
        ])->where('disposalId', $id)->get();
        
        $this->dataDisposalDetail = $dataDisposalDetail;
        // $this->disposalId = $dataDisposalDetail[0]->inventory->inventoryName;
        // dd($this->disposalId);

        // dd($dataDisposalDetail);

        $this->isModalDetailOpen = true;
    }

    public function closeDetailDisposal()
    {
        $this->isModalDetailOpen = false;
    }

    public function approveDisposal($id)
    {
        Gate::authorize('admin');
        // data disposal master
        $dataDisposal = Disposals::with([
            'user',
        ])->find($id);

        // dd($dataDisposal);
        $this->disposalId = $id;
        $this->disposalNumber = $dataDisposal->disposalNumber;
        $this->disposalDate = $dataDisposal->disposalDate;
        $this->description = $dataDisposal->description;
        $this->status = $dataDisposal->status;
        $this->userId = $dataDisposal->user->name;
        $this->inventory = $dataDisposal->totalInventory;

        // data disposal detail
        $dataDisposalDetail = DisposalDetails::with([
            'inventory',
            'disposal'
        ])->where('disposalId', $id)->get();

        $this->dataDisposalDetail = $dataDisposalDetail;
        
        $dataDisposalApproval = DisposalApprovals::with([
            'user',
        ])->where('disposalId', $id)->where('userId', Auth::user()->id)->get();

        $this->dataDisposalApproval = $dataDisposalApproval;
        
        $this->isModalApproveOpen = true;
    }

    public function cancelApproveDisposal()
    {
        $this->isModalApproveOpen = false;
    }

    public function storeAndUpdateApproveDisposal()
    {
        Gate::authorize('admin');

        $this->validate([
            'approvalComment' => 'required',
            // 'approvalSignature' => 'required',
        ], [
            'approvalComment.required' => 'Comment is required',
            // 'approvalSignature.required' => 'Signature is required',
        ]);
        
        // store signature
        // $folderPath = public_path('upload/images/signature/');

        // $image_parts = explode(";base64,", $this->approvalSignature);

        // $image_type_aux = explode("image/", $image_parts[0]);

        // $image_type = $image_type_aux[1];

        // $image_base64 = base64_decode($image_parts[1]);

        // $fileToFolder = $folderPath . date('ymd-hi') . '.'.$image_type;

        // $fileToDatabase = date('ymd-hi') . '.'.$image_type;

        // file_put_contents($fileToFolder, $image_base64);

        // $dataDisposalApproval = DisposalApprovals::find($this->disposalId);

        // dd($dataDisposalApproval);

        $checkDisposal = Disposals::findOrFail($this->disposalId);
        $checkDisposalApproval = DisposalApprovals::where('disposalId', $this->disposalId)->where('userId', Auth::user()->id)->get();

        // dd([
        //     $checkDisposal,
        //     $checkDisposalApproval[0]->status,
        // ]);
        
        if($checkDisposal->status == 'PENDING' && $checkDisposalApproval[0]->status == 'PENDING' && Auth::user()->roles = 'ADMIN') {
            $checkDisposal->update([
                'status' => 'ON PROGRESS'
            ]);

            $checkDisposalApproval[0]->update([
                'status' => 'APPROVE',
                'comment' => $this->approvalComment,
                // 'signature' => $this->approvalSignature,
            ]);

            DisposalApprovals::create([
                'disposalId' => $this->disposalId,
                'userId' => Auth()->user()->parentUserId,
                'status' => 'PENDING',
                'comment' => '',
                'signature' => '',
            ]);
            alert()->success('SuccessAlert','Disposal has been approved successfully.');
        } elseif($checkDisposal->status == 'ON PROGRESS' && $checkDisposalApproval[0]->status == 'PENDING' && Auth::user()->roles = 'SUPERADMIN') {
            $checkDisposal->update([
                'status' => 'DONE'
            ]);
            
            $checkDisposalApproval[0]->update([
                'status' => 'APPROVE',
                'comment' => $this->approvalComment,
                // 'signature' => $this->approvalSignature,
            ]);
            alert()->success('SuccessAlert','Disposal has been approved successfully.');
        } else {
            alert()->warning('WarningAlert','Disposal approval has been error.');
        }

        $this->isModalApproveOpen = false;
    }
}
