<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Wuwx\LaravelAutoNumber\AutoNumberTrait;

class WorkOrder extends Model
{
    use HasFactory, SoftDeletes;
    //  AutoNumberTrait;

    protected $table = 'work_orders';

    protected $fillable = [
        'workOrderNumber',
        'workOrderType',
        'workOrderStatus',
        'workOrderDescription',
        'userIdRequestedBy',
        'userIdApprovedBy',
        'userIdAssignedTo',
        'workOrderNumberRequestToServiceCenter',
        'workOrderDateRequest',
        'workOrderTroubleShootingNote',
        'workOrderDateUpdate',
    ];

    protected $hidden = [];

    public function userRequestedBy()
    {
        return $this->belongsTo(User::class, 'userIdRequestedBy');
    }

    public function workOrderTypes()
    {
        return $this->belongsTo(WorkOrderTypes::class, 'workOrderType');
    }

    public function workOrderApprovals()
    {
        return $this->hasMany(WorkOrderApproval::class, 'workOrderId', 'id');
    }

    public function workOrderHandlers()
    {
        return $this->hasMany(WorkOrderHandler::class, 'workOrderId', 'id');
    }

    // public function getAutoNumberOptions()
    // {
    //     return [
    //         'workOrderNumber' => [
    //             'format' => function () {
    //                 return 'WO/' . date('Ymd') . '/?'; // autonumber format. '?' will be replaced with the generated number.
    //             },
    //             'length' => 4, // The number of digits in the autonumber
    //         ],
    //     ];
    // }
}
