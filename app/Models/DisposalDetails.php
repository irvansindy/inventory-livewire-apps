<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class DisposalDetails extends Model
{
    use HasFactory, softDeletes;

    protected $table = 'disposal_details';

    protected $fillable = [
        'disposalId',
        'inventoryId',
        'description',
    ];

    protected $hidden = [];

    public function disposal()
    {
        return $this->belongsTo(Disposals::class, 'disposalId');
    }

    public function inventory()
    {
        return $this->belongsTo(ProductInventory::class, 'inventoryId');
    }
}
