<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class DisposalApprovals extends Model
{
    use HasFactory, softDeletes;

    protected $fillable = [
        'disposalId',
        'userId',
        'status',
        'comment',
        'signature',
    ];

    protected $hidden = [];

    public function disposal()
    {
        return $this->belongsTo(Disposals::class, 'disposalId');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }
}
