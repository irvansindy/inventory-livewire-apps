<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkOrderTypes extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'work_order_types';

    protected $fillable = [
        'workOrderType',
    ];

    protected $hidden = [];
}
