<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkOrderApproval extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'workOrderId',
        'userId',
        'dateApproved',
    ];

    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'userId', 'id');
    }

    public function workOrder()
    {
        return $this->belongsTo(WorkOrder::class, 'workOrderId', 'id');
    }
}
