<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Disposals extends Model
{
    use HasFactory, softDeletes;

    protected $table = 'disposals';

    protected $fillable = [
        'disposalNumber',
        'userId',
        'disposalDate',
        'totalInventory',
        'description',
        'status',
    ];

    protected $hidden = [];

    public function disposalDetails()
    {
        return $this->hasMany(DisposalDetails::class, 'disposalId');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }


}
