<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');

        // gate 
        Gate::define('admin', function(User $user) {
            // return $user->id === $post->user_id;
            return $user->roles !== 'USER' ?  Response::allow() : Response::deny('You must be an administrator.');
        });
        
        Gate::define('onlyAdmin', function(User $user) {
            // return $user->id === $post->user_id;
            return $user->roles == 'ADMIN' ?  Response::allow() : Response::deny('You must be an administrator.');
        });

        Gate::define('onlyUser', function(User $user){
            return $user->roles === 'USER'  ?  Response::allow() : Response::deny('You must be an user.');
            //&& $user->positionId !== 1
        });

        Gate::define('userHandler', function(User $user){
            return $user->roles === 'USER' && $user->positionId == 1 ? Response::allow() : Response::deny('You must be an user.');
        });
        
        Gate::define('allUser', function(User $user){
            return $user->roles === 'USER'? Response::allow() : Response::deny('You must be an user.');
        });
        
        Gate::define('userProcurement', function(User $user){
            return $user->roles === 'SUPERUSER' && $user->positionId == 2 ? Response::allow() : Response::deny('You must be part of procurement division.');
        });


    }
}
