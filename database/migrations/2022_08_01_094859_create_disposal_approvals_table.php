<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisposalApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disposal_approvals', function (Blueprint $table) {
            $table->id();
            $table->BigInteger('disposalId')->unsigned();
            $table->BigInteger('userId')->unsigned();
            $table->string('status');
            $table->string('comment')->nullable();
            $table->text('signature')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disposal_approvals');
    }
}
