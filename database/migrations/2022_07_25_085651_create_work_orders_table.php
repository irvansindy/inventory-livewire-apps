<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
            $table->id();
            $table->string('workOrderNumber');
            $table->biginteger('workOrderType');
            $table->enum('workOrderStatus', ['PENDING', 'ON PROGRESS', 'DONE']);
            $table->text('workOrderDescription');
            $table->biginteger('userIdRequestedBy');
            $table->biginteger('userIdApprovedBy')->nullable();
            $table->biginteger('userIdAssignedTo')->nullable();
            $table->string('workOrderNumberRequestToServiceCenter')->nullable();
            $table->date('workOrderDateRequest');
            $table->text('workOrderTroubleShootingNote')->nullable();
            $table->date('workOrderDateUpdate')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_orders');
    }
}
