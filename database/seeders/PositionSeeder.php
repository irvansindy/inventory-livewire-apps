<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'positionName' => 'ICT',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'positionName' => 'Procurement',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
