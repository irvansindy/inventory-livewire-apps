<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class WorkOrderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('work_order_types')->insert([
            [
                'id' => 1,
                'workOrderType' => 'Hardware',

            ],[
                'id' => 2,
                'workOrderType' => 'Software',
            ], [
                'id' => 3,
                'workOrderType' => 'Network',
            ], [
                'id' => 4,
                'workOrderType' => 'Other',
            ],
        ]);
    }
}
